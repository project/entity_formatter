<?php
/**
 * @file
 * class {{ class_name }}
 */

class {{ class_name }} extends {{ parent_class }} {
  public function __construct($entity) {
    $entity_type = '{{ entity_type }}';

    parent::__construct($entity_type, $entity);
  }
}