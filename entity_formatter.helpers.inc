<?php

/**
 * Wraps field value(s) with a HTML tag and optional CSS classes and field-label
 *   only when value is not empty.
 *
 * @param $formatter array | string A single value or array of strings or EfFieldFormatter objects.
 * @param $wrap string
 */
function entity_formatter_wrap($formatter, $wrap, $classes = '', $label = '', $item_wrap = '', $item_classes = '', $item_separator = '') {
  if ($formatter instanceof EfFieldFormatter) {
    // Convert EfFieldFormatter to a string.
    $formatter = (string) $formatter;
  }

  if ((is_array($formatter) && count($formatter)) || is_string($formatter) && $formatter != '' ) {
    $class = $classes != '' ? 'class="' . $classes . '"' : '';
    $item_class = $item_classes != '' ? 'class="' . $item_classes . '"' : '';

    if ($label != '') {
      $label = '<label>' . $label . '</label>';
    }
    if (is_array($formatter)) {
      $items = array();
      foreach ($formatter as $item) {
        $items[] = $item_wrap != '' ? "<$item_wrap $item_class>$item</$item_wrap>" : $item;
      }
      $formatter = implode($item_separator, $items);
    }
    else if ($item_wrap != '') {
      $formatter = "<$item_wrap $item_class>$formatter</$item_wrap>";
    }
    return "<$wrap $class>" . $label . $formatter . "</$wrap>";
  }
}

/**
 * Returns the field with default formatter with a label if there is a value.
 *
 * @param $formatter EfFieldFormatter
 * @param $wrap string
 */
function entity_formatter_label($formatter, $label) {
  if ($formatter->isDeltaValid()) {
    $label = '<label>' . $label . '</label>';
    return $label . $formatter;
  }
}